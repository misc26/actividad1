# Actividad 1

El archivo LaTeX main.tex contiene el resultado
y el procedimiento de las operaciones. El 
encabezado y estructura del documento fueron 
escritos a mano, sin embargo, las operaciones
en sí fueron generadas por un script de awk.

Para generar las operaciones y agregarlas
al documento utilizar el comando:
```bash
cat data.csv | awk -f inciso-1.awk >> solution.tex
```
sustituir el numero del inciso en el comando anterior de acuerdo
al inciso que se este realizando.

Luego editar el archivo solution.tex para colocar
las operaciones donde deben ir. <br>
Para compilar el documento utilizar el comando:

```bash
pdflatex -synctex=1 -interaction=nonstopmode solution.tex
```
