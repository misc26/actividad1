BEGIN{PI=3.141516}
function square(num){return num * num}
function op(carga) { return carga / (PI*square(1.016)) }
{gsub(/%/,"")}
{
	print "\t\\item $\\frac{"$1"}{\\Pi*1.016^2} = "op($1)"$"
}
